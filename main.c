#include <stdio.h>
#include <stdlib.h>
#include<stdint.h>
typedef enum
{
    czestotliwosc,
    natezenie=220,
    napiecie,
    rezystancja
} wejscia;
typedef union {
    int czteryB;
    uint8_t x[4];

} unia;
int suma(int *skladnik1,int *skladnik2);
wejscia funkcjadlaenum(void);
typedef struct
{
    int ilosc;
    int jakosc;

}parametry;





int main(void)
{
    //czesc programu dla wskaznikow
    int *x;
    int a,b,sum;
    printf("Podaj pierwsza liczbe: ");
    scanf("%d", &a);
    printf("Podaj druga liczbe: ");
    scanf("%d", &b);
    sum = suma(&a,&b);
    printf("Suma tych liczb jest rowna: %d\n", sum);

    //czesc programu dla struktur
    parametry jeden;
    printf("Podaj ilosc parametru 1:\n");
    scanf("%d", &jeden.ilosc);
    printf("Podaj jakosc podanego parametru (%): \n");
    scanf("%d", &jeden.jakosc);
    printf("Ilosc= %d, Jakosc= %d.\n",jeden.ilosc,jeden.jakosc);
    zamiana(&jeden.ilosc,&jeden.jakosc);
    printf("Teraz Ilosc= %d, Jakosc= %d.\n",jeden.ilosc,jeden.jakosc);
    printf("To chyba pomylka.\n");



    printf("%d",funkcjadlaenum());

    unia unia;
    unia.czteryB = 1;
    printf("%d\n",unia.x[0]);
    printf("%d\n",unia.x[1]);
    printf("%d\n",unia.x[2]);
    printf("%d\n",unia.x[3]);
    uint8_t *w1= &unia.x[0];
    uint8_t *w2= &unia.x[1];
    uint8_t *w3= &unia.x[2];
    uint8_t *w4= &unia.x[3];
    printf("w1 %d\n",w1);
    printf("w2 %d\n",w2);
    printf("w3 %d\n",w3);
    printf("w4 %d\n",w4);


}

//funkcja dodawania na wskaznikach
int suma(int *skladnik1,int *skladnik2)
{
    return *skladnik1 + *skladnik2;
}

//zamiana wartosci pol struktury
void zamiana(parametry *data)
{
    int temp;
    temp = data->ilosc;
    data->ilosc = data->jakosc;
    data->jakosc = temp;
}

wejscia funkcjadlaenum(void)
{
    return natezenie;
}










